package bootcamp.project.gsd.controller;

import bootcamp.project.gsd.dto.TodoRequest;
import bootcamp.project.gsd.entity.Todo;
import bootcamp.project.gsd.repository.TodoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private TodoRepository todoRepository;
    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }


    @Test
    void should_return_todos_when_perform_get_all_given_todos() throws Exception {
        Todo read = new Todo("read", false);
        Todo sport = new Todo("sport", false);
        todoRepository.saveAll(List.of(read, sport));

        mockMvc.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(read.getText()))
                .andExpect(jsonPath("$[1].text").value(sport.getText()));
    }

    @Test
    void should_todo_when_perform_post_given_todo() throws Exception {
        Todo test = new Todo("test add", false);

        mockMvc.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(test)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value(test.getText()));
    }

    @Test
    void should_todo_when_perform_put_given_todo_request() throws Exception {
        Todo test = new Todo("test", false);
        Todo savedTodo = todoRepository.save(test);
        TodoRequest todo = new TodoRequest("test aa", false);

        mockMvc.perform(MockMvcRequestBuilders.put("/todos/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todo)))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.text").value(todo.getText()));
    }


    @Test
    void should_return_no_content_when_perform_delete_todo_given_todo_id() throws Exception {
        Todo test = new Todo("test", false);
        Todo savedTodo = todoRepository.save(test);

        mockMvc.perform(MockMvcRequestBuilders.delete("/todos/{id}", savedTodo.getId()))
                .andExpect(status().isNoContent());

        Todo todo = todoRepository.findById(savedTodo.getId()).orElse(null);

        Assertions.assertNull(todo);
    }

    @Test
    void should_return_todo_when_perform_get_all_given_todo_id() throws Exception {
        Todo read = new Todo("read", false);
        Todo savedTodo = todoRepository.save(read);

        mockMvc.perform(MockMvcRequestBuilders.get("/todos/{id}", savedTodo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(savedTodo.getId()))
                .andExpect(jsonPath("$.text").value(savedTodo.getText()));
    }
}
