package bootcamp.project.gsd.repository;

import bootcamp.project.gsd.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Integer> {
}
