package bootcamp.project.gsd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TodoRequest {
    private String text;

    private Boolean done;
}
