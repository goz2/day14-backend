package bootcamp.project.gsd.dto;

import lombok.Data;

@Data
public class TodoResponse {
    private Integer id;

    private String text;

    private Boolean done;
}
