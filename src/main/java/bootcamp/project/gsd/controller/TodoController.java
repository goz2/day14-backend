package bootcamp.project.gsd.controller;

import bootcamp.project.gsd.dto.TodoRequest;
import bootcamp.project.gsd.dto.TodoResponse;
import bootcamp.project.gsd.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/todos")
public class TodoController {
    private final TodoService todoService;

    @GetMapping
    public List<TodoResponse> getTodoList() {
        return todoService.getTodoList();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse addTodo(@RequestBody TodoRequest todoRequest) {
        return todoService.addTodo(todoRequest);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public TodoResponse editTodo(@PathVariable Integer id, @RequestBody TodoRequest todoRequest) {
        return todoService.editTodo(id, todoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Integer id) {
        todoService.deleteTodo(id);
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Integer id) {
        return todoService.getTodoById(id);
    }
}
