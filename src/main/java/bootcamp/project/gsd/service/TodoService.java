package bootcamp.project.gsd.service;

import bootcamp.project.gsd.dto.TodoRequest;
import bootcamp.project.gsd.dto.TodoResponse;
import bootcamp.project.gsd.entity.Todo;
import bootcamp.project.gsd.exception.NotFoundException;
import bootcamp.project.gsd.mapper.TodoMapper;
import bootcamp.project.gsd.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static bootcamp.project.gsd.mapper.TodoMapper.toEntity;
import static bootcamp.project.gsd.mapper.TodoMapper.toResponse;

@Service
@RequiredArgsConstructor
public class TodoService {
    private final TodoRepository todoRepository;

    public List<TodoResponse> getTodoList() {
        return todoRepository.findAll().stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public TodoResponse addTodo(TodoRequest todoRequest) {
        Todo savedTodo = todoRepository.save(toEntity(todoRequest));
        return toResponse(savedTodo);
    }

    public TodoResponse editTodo(Integer id, TodoRequest todoRequest) {
        Todo todo = todoRepository.findById(id).orElseThrow(NullPointerException::new);
        BeanUtils.copyProperties(todoRequest, todo);
        Todo savedTodo = todoRepository.save(todo);
        return toResponse(savedTodo);
    }

    public void deleteTodo(Integer id) {
        todoRepository.deleteById(id);
    }

    public TodoResponse getTodoById(Integer id) {
        return toResponse(todoRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
