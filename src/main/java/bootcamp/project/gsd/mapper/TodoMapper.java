package bootcamp.project.gsd.mapper;

import bootcamp.project.gsd.dto.TodoRequest;
import bootcamp.project.gsd.dto.TodoResponse;
import bootcamp.project.gsd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        return todo;
    }
}
