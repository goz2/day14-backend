package bootcamp.project.gsd.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("todo not found.");
    }
}
