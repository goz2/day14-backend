package bootcamp.project.gsd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsdApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsdApplication.class, args);
	}

}
