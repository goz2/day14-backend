drop table if exists `todo`;

CREATE TABLE `todo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(150) NOT NULL,
  `done` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`));
